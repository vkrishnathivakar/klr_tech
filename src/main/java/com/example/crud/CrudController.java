package com.example.crud;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CrudController {

	@Autowired
	private EmployeeService service;

	@GetMapping("/")
	public String viewHomePage(Model model) {
		List<Employee> listEmployees = service.listAll();
		model.addAttribute("listEmployees", listEmployees);
		return "index";
	}

	@GetMapping("/new")
	public String addNewEmployeePage(Model model) {
		Employee employee = new Employee();
		model.addAttribute("employee", employee);
		List<Employee> listEmployees = service.listAll();
		model.addAttribute("listEmployees", listEmployees);
		return "new_employee";
	}

	@PostMapping("/save")
	public String saveProduct(@ModelAttribute("employee") Employee employee) {
		service.save(employee);
		return "redirect:/";
	}

	@GetMapping("/edit/{id}")
	public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
		ModelAndView mv = new ModelAndView("edit_employee");
		Employee employee = service.get(id);
		mv.addObject("employee", employee);
		List<Employee> listEmployees = service.listAll();
		mv.addObject("listEmployees", listEmployees);
		return mv;
	}

	@GetMapping("/delete/{id}")
	public String deleteEmployee(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/";
	}

	@GetMapping("/get/lead/{id}")
	public ModelAndView showLeadData(@PathVariable(name = "id") int id) {
		ModelAndView mv = new ModelAndView("lead");
		Lead leadData = service.getLead(id);
		mv.addObject("lead", leadData);
		return mv;
	}

}
