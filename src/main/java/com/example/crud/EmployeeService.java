package com.example.crud;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	private EmployeeRepository repo;

	public List<Employee> listAll() {
		return repo.findAll();
	}

	public void save(Employee employee) {
		repo.save(employee);
	}

	public Employee get(long id) {
		return repo.findById(id).get();
	}

	public void delete(long id) {
		repo.deleteById(id);
	}

	public Lead getLead(long id) {
		Lead listLead = new Lead();
		Employee emp = repo.findById(id).get();
		Employee lead = repo.findById(emp.getLeadId()).get();
		listLead.setName(emp.getName());
		listLead.setLeadName(lead.getName());
		return listLead;
	}

}
